from unittest import TestCase
from Process_Sequence import  Process_Sequence

class Process_Sequence_Test(TestCase):
    ##Iteracion 1. Devolver Arrego con No. Elementos
    '''
    def test_process(self):
        s = Process_Sequence()
        self.assertEquals(s.process(''),'','Iteración 1. Cadena vacía')
    def test_process_Iteracion1_Numero1(self):
        s = Process_Sequence()
        self.assertEquals(s.process('1'),[1],'Iteración 1. Numero 1')
    def test_process_Iteracion1_Numero2(self):
        s = Process_Sequence()
        self.assertEquals(s.process('1,2'),[2],'Iteración 1. Numero 2')
    def test_process_Iteracion1_NumeroN(self):
        s = Process_Sequence()
        self.assertEquals(s.process('1,2,3,4'),[4],'Iteración 1. Numero N')
    '''
##Iteracion 2. Devolver Arreglo con No. Elementos y Minimo
    def test_process_Iteracion2_NumeroN(self):
        s = Process_Sequence()
        self.assertEquals(s.process(''),[0,''],'Iteración 2. Cadena Vacía')
    def test_process_Iteracion2_Numero1(self):
        s = Process_Sequence()
        self.assertEquals(s.process('1'),[1,1],'Iteración 2. Numeros: 1')
    def test_process_Iteracion2_Numero2(self):
        s = Process_Sequence()
        self.assertEquals(s.process('1,2'),[2,1],'Iteración 2. Numeros: 2')
    def test_process_Iteracion2_NumeroN(self):
        s = Process_Sequence()
        self.assertEquals(s.process('2,3,4,5,6'),[5,2],'Iteración 2. Numeros: N')

